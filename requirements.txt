click>=6.7
Django>=2.0.1
python-magic>=0.4.15
google-api-python-client>=1.6.2
oauth2client==4.1.3
pycryptodome==3.15.0

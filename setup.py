from setuptools import setup, find_packages

setup(
    name="vortex",
    version="1",
    packages=find_packages(),
    entry_points={
        "console_scripts": ["vortex = vortex.upload:upload"]
        },
    install_requires=[
        'pycryptodome==3.15.0',
        'python-magic==0.4.15',
        'google-api-python-client==2.65.0',
        'pyperclip==1.6.0',
        'hurry.filesize==0.9',
        'httplib2==0.15.0' # See https://github.com/googleapis/google-api-python-client/issues/803
        ]
    )

import os, random, struct
from Crypto.Cipher import AES
from Crypto.Random import random


class Encryptor():
    def __init__(self, key: str):
        self.iv = bytes(random.getrandbits(8) for _ in range(16))
        self.encryptor = AES.new(key, AES.MODE_CBC, self.iv)

    def encrypt_chunk(self, chunk):
        return self.encryptor.encrypt(chunk)

    @staticmethod
    def to_chunk_size(size: int) -> int:
        return size - (size % 16)

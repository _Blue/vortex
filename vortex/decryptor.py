import os, random, struct
from Crypto.Cipher import AES
from Crypto.Random import random


class Decryptor():
    def __init__(self, key: str, iv: str):
        self.encryptor = AES.new(key, AES.MODE_CBC, iv)

    def decrypt_chunk(self, chunk):
        return self.encryptor.decrypt(chunk)

    @staticmethod
    def to_chunk_size(size: int) -> int:
        return size - (size % 16)

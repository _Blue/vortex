import threading
import magic
import mimetypes
from . import settings
from django.http import StreamingHttpResponse, HttpResponseNotFound
from .gdrive import GDrive, FileNotFoundException
from .credentials import get_credentials

thread_local = threading.local()
thread_local.service = None

def get_service():
    service = getattr(thread_local, 'service', None)
    if service:
        return service

    credentials = get_credentials(settings.GDRIVE_CREDENTIALS, None)
    service = GDrive(credentials, settings.ENCRYPTION_KEY)
    setattr(thread_local, 'service', service)
    print("Initialized thread: " + threading.current_thread().name)

    return service

def suspicious_file_name(name: str) -> bool:
    return any(e in name for e in ["'", '"', '\\', '/'])

def get_mime_type(content) -> str:
    return magic.Magic(mime=True).from_buffer(next(content.__iter__()))

def fill_metadata(response, file, id: str):
    mime_type = get_mime_type(file)

    response['Content-Length'] = file.size
    response['Content-Type'] = mime_type
    if mime_type.startswith('image'):
        return

    file_extension = mimetypes.guess_extension(mime_type) or ""
    name = id + file_extension
    response['Content-Disposition'] = 'Attachement; filename="{}"'.format(name)


def get_file(request, name: str):
    if not name or suspicious_file_name(name):
        return HttpResponseNotFound("File not found")

    try:
        file = get_service().get_file(name)
        response = StreamingHttpResponse(file)
        fill_metadata(response, file, name)
        return response

    except FileNotFoundException:
        return HttpResponseNotFound("File not found")

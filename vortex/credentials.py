import os
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage

SCOPES = 'https://www.googleapis.com/auth/drive'
CLIENT_SECRET_PATH = os.path.join(os.path.expanduser("~"), ".config", "vortex", "client_secret.json")
CREDENTIALS_PATH = os.path.join(os.path.expanduser("~"), ".config", "vortex", "credentials.json")
APPLICATION_NAME = 'Vortex'

def get_credentials(cred_path=CREDENTIALS_PATH, secret_path=CLIENT_SECRET_PATH):
    store = Storage(cred_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        if not secret_path:
            raise RuntimeError("Failed to find valid credentials to access Google Drive")

        flow = client.flow_from_clientsecrets(secret_path, SCOPES)
        credentials = tools.run_flow(flow, store)
        print('Storing credentials to ' + cred_path)
    return credentials


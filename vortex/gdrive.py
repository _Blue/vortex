#! /usr/bin/python3
import httplib2
import io
import os
import sys
import traceback
import struct
import magic
from .decryptor import Decryptor
from .credentials import get_credentials
from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
from googleapiclient.http import MediaIoBaseDownload



class FileNotFoundException(RuntimeError):
    pass

class GFile:
    def __init__(self, service, id: str):
        self.service = service
        self.id = id

    def __iter__(self):
        request = self.service.files().get_media(fileId=self.id)

        with io.BytesIO() as stream:
            downloader = MediaIoBaseDownload(stream, request, chunksize=1024*1024*8)

            done = False
            while not done:
                stream.seek(0)
                stream.truncate()

                status, done = downloader.next_chunk()

                content = stream.getvalue()
                assert len(content) % 16 == 0

                yield content


class EncryptedGFile(GFile):
    def __init__(self, key, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.key = key
        self.pos = 0

        self.process_header(next(super(EncryptedGFile, self).__iter__()))

    def process_header(self, chunk):
        assert len(chunk) >= 32 and len(chunk) % 16 == 0
        self.size = struct.unpack("Q", chunk[:8])[0]
        iv = chunk[8:24]

        self.decryptor = Decryptor(self.key, iv)

        self.first_chunk = self.remove_padding(self.decryptor.decrypt_chunk(chunk[32:]))


    def remove_padding(self, chunk):
        extra = self.pos +  len(chunk) - self.size

        return chunk[:len(chunk) - extra]

    def __iter__(self):
        it = super(EncryptedGFile, self).__iter__()
        self.process_header(next(it))

        self.pos = 0
        yield self.first_chunk
        self.pos += len(self.first_chunk)

        e = next(it, None)
        while e:
            assert len(e) % 16 == 0

            yield self.remove_padding(self.decryptor.decrypt_chunk(e))
            self.pos += len(e)
            e = next(it, None)

    @property
    def mime_type(self):
        return magic.Magic(mime=True).from_buffer(next(self.__iter__()))

class GDrive:
    def __init__(self, credentials, key):
        http = credentials.authorize(httplib2.Http())
        self.service = discovery.build('drive', 'v3', http=http)
        self.key = key

    def file_from_path(self, name: str, type=None, parent=None):
        assert "'" not in name # Cheap injection protection

        query = "name = '{}' and trashed = false".format(name)
        if type:
            query += " and mimeType = '{}'".format(type)

        if parent:
            query += " and '{}' in parents".format(parent)

        result = self.service.files().list(q=query).execute()

        files = [e for e in result['files'] if e['name'] == name]
        assert len(files) < 2

        if not files:
            raise FileNotFoundException()
        return files[0]

    def get_file_impl(self, id: str):
        return EncryptedGFile(self.key, self.service, id)

    def get_file(self, path: str, parent= None):
        file = self.file_from_path(path, parent=parent)
        if file['mimeType'] == 'application/vnd.google-apps.folder':
            raise FileNotFoundException()

        return self.get_file_impl(file['id'])

    def get_folder_id(self, name: str, parent=None) -> str:
        return self.file_from_path(name, type='application/vnd.google-apps.folder')['id']

    def get_or_create_folder(self, name: str, parent=None) -> str:
        try:
            return self.file_from_path(name, parent)
        except FileNotFoundException:
            return self.create_folder(name, parent)

    def create_folder(self, name: str, parent= None) -> str:
        metadata = {
            'name': name,
            'mimeType': 'application/vnd.google-apps.folder'
            }

        if parent:
            metadata['parents'] = [parent]

        result = self.service.files().create(body=metadata, fields='id').execute()

        return result.get('id')

    def file_exists(self, name: str, parent=None) -> str:
        try:
            self.file_from_path(name, parent)
            return True
        except FileNotFoundException:
            return False

    def delete(self, id: str) -> str:
        self.service.files().delete(fileId=id).execute()

    def delete_by_name(self, name: str, parent=None) -> str:
        self.delete(self.file_from_path(name, parent=parent)['id'])

    def delete_folder_by_name(self, name: str, parent= None) -> str:
        self.delete(self.get_folder_id(name, parent=parent))

    def empty_trash(self):
        self.service.files().emptyTrash()

    def list_all_files(self, fields="files(id, name, size, createdTime)") -> list:
        query = "mimeType != 'application/vnd.google-apps.folder'" # Exclude folders
        result = self.service.files().list(fields=fields, q=query).execute()

        files = result.get("files", [])
        token = result.get("nextPageToken", None)
        while token:
            result = self.service.files().list(nextPageToken=token, fields=fields).execute()

            files += result.get("files", [])
            token = result.get("nextPageToken", None)

        return files


    @staticmethod
    def build(key):
        assert key
        return GDrive(get_credentials(), key)

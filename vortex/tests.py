import os
import io
import tempfile
import click
import base64
import zlib
from . import settings
from django.test import Client
from django.test import TestCase
from .upload import upload_impl
from .gdrive import GDrive, FileNotFoundException
from django.test.runner import DiscoverRunner
from PIL import Image

TEST_FOLDER_NAME = "vortex_tests"

config = {'key': base64.b64encode(os.urandom(32))}
settings.ENCRYPTION_KEY = base64.b64decode(config["key"])

class DatabaselessTestRunner(DiscoverRunner):
    """A test suite runner that does not set up and tear down a database."""

    def setup_databases(self, *args, **kwargs):
        pass

    def teardown_databases(self, *args):
        pass

def init_test_folder(service, name):
    try:
        service.delete_folder_by_name(name)
    except FileNotFoundException:
        pass

    return service.create_folder(name)


def upload_file(content, service, config, root, name="test_file"):
        file = io.BytesIO(content)
        dest = tempfile.NamedTemporaryFile()
        setattr(file, "name", "dummy")


        upload_impl(file, dest, config, name, service, parent=root)



class FileUpload(TestCase):
    def setUp(self):
        self.drive = GDrive.build(key=base64.b64decode(config["key"]))

    def upload(self, content):
        root = init_test_folder(self.drive, TEST_FOLDER_NAME)

        upload_file(content, self.drive, config, root)

        download = self.drive.get_file("test_file", parent=root)

        self.assertEqual(download.size, len(content))

        self.check_content(download, content)

        self.drive.delete(root)

    def check_content(self, download, content):
        result = bytes()

        for e in download:
            result += e

        self.assertEqual(result, content)

    def test_small_upload(self):
        self.upload(b"Dummy")

    def test_average_upload(self):
        self.upload(b"Average" * 10)

    def test_aglined_upload(self):
        self.upload(b"K" * 16)

    def test_very_big_upload(self):
        self.upload(b"VeryBigFile" * 1024 * 1024)


class Query(TestCase):
    def setUp(self):

        self.drive = GDrive.build(key=base64.b64decode(config["key"]))
        self.root = init_test_folder(self.drive, TEST_FOLDER_NAME)
        self.client = Client()


    def test_basic_download(self):
        content = zlib.compress(b'basic.download.normal.file.size.over.than.16.bytes()')
        upload_file(content, self.drive, config, self.root, "basic")

        response = self.client.get('/basic')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Length'], str(len(content)))

        downloaded = bytes()
        for e in response:
            downloaded += e

        self.assertEqual(downloaded, content)
        self.assertEqual(response['Content-Disposition'], 'Attachement; filename="basic"')

    def test_not_found(self):
        response = self.client.get('/nope')

        self.assertEqual(response.status_code, 404)

    def test_folder(self):
        response = self.client.get('/' + TEST_FOLDER_NAME)

        self.assertEqual(response.status_code, 404)

    def test_quotes(self):
        response = self.client.get("/file'")

        self.assertEqual(response.status_code, 404)

    def test_quotes2(self):
        response = self.client.get("/'")

        self.assertEqual(response.status_code, 404)

    def test_quotes3(self):
        response = self.client.get("/'file'")

        self.assertEqual(response.status_code, 404)

    def test_backslash(self):
        response = self.client.get("/file\\")

        self.assertEqual(response.status_code, 404)

    def backslash2(self):
        response = self.client.get("/\\")

        self.assertEqual(response.status_code, 404)

    def test_image(self):
        image = Image.new('RGB', (1, 1))

        buffer = io.BytesIO()
        image.save(buffer, format="png")
        content = buffer.getvalue()

        upload_file(content, self.drive, config, self.root, "image")

        response = self.client.get('/image')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Length'], str(len(content)))

        downloaded = bytes()
        for e in response:
            downloaded += e

        self.assertEqual(downloaded, content)

        self.assertTrue('Content-Disposition' not in response)
        self.assertEqual(response['Content-Type'], 'image/png')


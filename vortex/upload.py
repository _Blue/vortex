#! /usr/bin/python3

import threading
import click
import base64
import configparser
import os
import hashlib
import tempfile
import struct
import magic
import tempfile
import traceback
import pdb
import pyperclip
import hurry.filesize

from vortex.gdrive import GDrive, FileNotFoundException
from vortex.encryptor import Encryptor
from googleapiclient.http import MediaIoBaseUpload
from subprocess import check_call


@click.command()
@click.argument('path', required=False)
@click.option('--name', '-n', default=None)
@click.option('--overwrite', '-o', is_flag=True)
@click.option('--list-files', '-l', is_flag=True)
@click.option('--delete', '-d', is_flag=True)
def upload(path=None, name=None, overwrite=False, list_files=False, delete=False):
    config = get_config()
    service = GDrive.build(base64.b64decode(config["key"]))

    if list_files:
        display_file_list(service, config)
        return
    elif delete:
        delete_file(service, path)
        return

    if name is not None:
        check_filename(service, name, overwrite)

    with open_target(path) as source, tempfile.NamedTemporaryFile() as dest:
        if name is None:
            name = hash_file(source, config)
            check_filename(service, name, overwrite)

        uri = config.get("host") + "/" + name
        try:
            pyperclip.copy(uri)
        except Exception as e:
            click.secho("Failed to copy URI to clipboard, " + traceback.format_exc(), fg="yellow")
        click.secho("URI: {} in clipboard".format(uri), bold=True)

        upload_impl(source, dest, config, name, service)

def display_file_list(service, config):
    files = service.list_all_files()
    for e in files:
        click.secho(e["name"], bold=True)

        click.secho("\tType: " + service.get_file_impl(e["id"]).mime_type)
        click.secho("\tSize: " + hurry.filesize.size(int(e["size"])))
        click.secho("\tCreated: " + e["createdTime"])
        click.secho("\tURI: {}/{}".format(config["host"], e["name"]))

    click.secho("Total size: " + hurry.filesize.size(sum(int(e["size"]) for e in files)))

def delete_file(service, name: str):
    if not name:
        click.secho("Option --delete requires a file name", fg="red")
        raise click.Abort()

    service.delete_by_name(name)
    click.secho("File: {} deleted".format(name))

    service.empty_trash()
    click.secho("Trash cleaned")

def check_filename(service, name: str, overwrite: bool):
    if not service.file_exists(name):
        return

    if overwrite:
        service.delete_by_name(name)
    else:
        click.secho("Filename: '%s' already exists, not overwriting (use --overwrite)" % name, fg="red")
        raise click.Abort()

def take_screenshot():
    file = tempfile.NamedTemporaryFile(suffix=".jpg")
    check_call(["scrot", "-s", file.name, '-o'])

    return file

def open_target(path: str):
    if path is None:
        return take_screenshot()

    return open(path, "rb")

def upload_impl(source, dest, config: dict, name: str, service, parent=None):
    encrypt_file(base64.b64decode(config["key"]), source, dest)

    dest.seek(0, 0)
    upload_file(dest, service.service, name, parent)


def file_size(source) -> int:
    source.seek(0, os.SEEK_END)
    size = source.tell()
    source.seek(0, os.SEEK_SET)

    return size

def read_file_by_chunks(file, chunk: int):
    while True:
        content = file.read(chunk)
        if not content:
            break

        yield content

def hash_file(source, config) -> str:
    print("Hash file: '{}'".format(source.name))

    chunk_size = config.get("hash_buffer", 1024 * 1024 * 4)
    hash = hashlib.sha1()
    with click.progressbar(length=file_size(source)) as bar:
        for e in read_file_by_chunks(source, chunk_size):
            hash.update(e)
            bar.update(chunk_size)

    return hash.hexdigest()

def encrypt_file(key: str, source, dest):
    print("Encrypt file: '{}', destination: '{}'".format(source.name, dest.name))

    cryptor = Encryptor(key)

    dest.write(struct.pack("Q", file_size(source))) # Write file size first
    dest.write(cryptor.iv)
    dest.write(struct.pack("Q", 0)) # 0 padding

    chunk_size = Encryptor.to_chunk_size(1024 * 1024 * 4)

    with click.progressbar(length=file_size(source)) as bar:
        for chunk in read_file_by_chunks(source, chunk_size):
            if len(chunk) % 16 != 0:
                chunk = bytes.ljust(chunk, len(chunk) + (16 - len(chunk) % 16))

            dest.write(cryptor.encrypt_chunk(chunk))
            bar.update(chunk_size)

def upload_file(source, service, name: str, parent: None):
    print("Upload file")
    media = MediaIoBaseUpload(source, mimetype="application/octet-stream", resumable=True)

    meta = {
            'name': name,
            'parents': [parent] if parent else []
           }

    request = service.files().create(body=meta,
                                     media_body=media,
                                     fields='id')

    response = None
    with click.progressbar(length=100) as bar:
        while response is None:
            status, response = request.next_chunk()
            if status:
                bar.pos = status.progress() * 100
                bar.render_progress()

        bar.pos = 100
        bar.render_progress()


def get_config() -> configparser.ConfigParser:
    config = configparser.ConfigParser()

    config.read(os.path.join(os.path.expanduser("~"), ".config", "vortex", "vortex.ini"))

    return config["vortex"]

if __name__ == '__main__':
    try:
        upload()
    except Exception as e:
        traceback.print_exc()

        if get_config().get("debug", False):
            pdb.post_mortem()
